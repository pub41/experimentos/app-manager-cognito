const create = require('./aws-cognito-create');
const update = require('./aws-cognito-update')
const login = require('./aws-cognito-login')

module.exports = {
  update,
  create,
  login
}
