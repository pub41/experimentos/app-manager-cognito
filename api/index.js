require('dotenv').config()
const express = require('express');
const routes = require('./router/index');

const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  }),
);

app.use(routes);

app.listen(26200, () => console.log(`Runing in the port 26200`));
