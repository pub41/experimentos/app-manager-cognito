const AmazonCognitoIdentity = require('amazon-cognito-identity-js')
const CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool
const AWS = require('aws-sdk')
const responseWithStatusCode = require('../../../services/utils/responseWithStatusCode')

const poolData = {
  UserPoolId : process.env.USER_POOL_ID, // Your user pool id here
  ClientId : process.env.CLIENT_ID // Your client id here
}

const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData)

const register = (email, password, attribute_list) => {
  return new Promise((resolve, reject) => {
    userPool.signUp(email, password, attribute_list, null, (err, result) => {
      if (err) {
        reject(err.message)
        return
      }
      resolve({username: result.user})
    })
  })
}

const registerUser = async (username, password) => {

    if (!username) {
      return 'username is not defined'
    }

    let attributeList = []

    let user = [
      {Name:'name',Value:'Teste Jayashanka'},
      {Name:'preferred_username',Value:'jay'},
      {Name:'gender',Value:'male'},
      {Name:'birthdate',Value:'1991-06-21'},
      {Name:'address',Value:'CMB'},
      {Name:'email',Value: username},
      {Name:'phone_number',Value:'+5412614324321'}
    ]

    user.map(item => {
      attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute(item))
    })

    return register(username, password, attributeList)
          .then(response => {
            return responseWithStatusCode(response, 201)
          }).catch(error => {
            return responseWithStatusCode(error, 500)
          })
}

module.exports = registerUser
