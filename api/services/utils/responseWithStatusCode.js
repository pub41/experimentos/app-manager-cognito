
module.exports = (response, status_code) => {
  return {'status_code': status_code, response}
}
