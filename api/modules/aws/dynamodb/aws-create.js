var AWS = require("aws-sdk");

const awsConfig = {
    "region": "us-east-1",
    "endpoint": process.env.ENDPOINT,
    "accessKeyId": process.env.ACCESS_KEY_ID,
    "secretAccessKey": process.env.SECRET_ACCESS_KEY
};

AWS.config.update(awsConfig);

const docClient = new AWS.DynamoDB.DocumentClient();

const create = (username) => {

  var params = {
      TableName: "dev-zelo-02",
      Item: {
          "email": username,
          roles: {
          "name": "Caixa",
          "actions": [
            { "id": 1, "name": "read" },
            { "id": 2, "name": "update" }
          ]},
          "created_at": new Date().toString(),
          "updated_at": new Date().toString(),
          "deleted_at": false,
      }
  }

  docClient.put(params, function (err, data) {

        if (err) {
            console.log("users::save::error - " + JSON.stringify(err, null, 2));
        } else {
            console.log("users::save::success" );
        }
    });
}

module.exports = create
