const AmazonCognitoIdentity = require('amazon-cognito-identity-js')
const CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool
const AWS = require('aws-sdk')
const responseWithStatusCode = require('../../../services/utils/responseWithStatusCode')

const poolData = {
  UserPoolId : process.env.USER_POOL_ID, // Your user pool id here
  ClientId : process.env.CLIENT_ID // Your client id here
}

const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData)
var cognitoUser = new AWS.CognitoIdentityServiceProvider({
  region: process.env.AWS_REGION
})


const promiseUpdateUser = (userData) => {
  return new Promise((resolve, reject) => {
    cognitoUser.updateUserAttributes(userData, (err, result) => {
        if (err) {
          console.log(err.message)
          reject(err.message)
        } else {
          resolve(result)
        }
    })
  })
}

const updateUser = (username, password, token) => {
        let attributeList = [
          {Name:'name',Value:'Marcelo Viana Almeida'},
          {Name:'preferred_username',Value:'Marcelo'},
          {Name:'gender',Value:'male'},
          {Name:'birthdate',Value:'1985-06-06'},
          {Name:'address',Value:'Estr. Sidney x'},
          {Name:'email',Value: username},
          {Name:'phone_number',Value:'+5412614324321'}
        ]

        attributeList.map(item => {
          attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute(item))
        })

        var userData = {
            UserAttributes: attributeList,
            AccessToken: token
        }

        return promiseUpdateUser(userData)
              .then(response => {
                return responseWithStatusCode(response? 'Success': '', 200)
              }).catch(error => {
                return responseWithStatusCode(error, 500)
              })
}

module.exports = updateUser
