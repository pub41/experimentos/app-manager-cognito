const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;
const AWS = require('aws-sdk');
const responseWithStatusCode = require('../../../services/utils/responseWithStatusCode')

const poolData = {
  UserPoolId : process.env.USER_POOL_ID, // Your user pool id here
  ClientId : process.env.CLIENT_ID // Your client id here
};

const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData)

const loginPromise = (cognitoUser, authenticationDetails) => {
  return new Promise((resolve, reject) => {

    cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: function (result) {
            // console.log('access token + ' + result.getAccessToken().getJwtToken());
            // console.log('id token + ' + result.getIdToken().getJwtToken());
            // console.log('refresh token + ' + result.getRefreshToken().getToken());
            resolve(result)
        },
        onFailure: function(err) {
            console.log(err);
            reject(err.message)
        },
    })

  });
}

const login = async (username, password) => {
    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
        Username : username,
        Password : password,
    });

    var userData = {
        Username : username,
        Pool : userPool
    };
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

    return loginPromise(cognitoUser, authenticationDetails)
          .then(response => {
            return responseWithStatusCode(response, 201)
          }).catch(error => {
            return responseWithStatusCode(error, 500)
          })

}

module.exports = login
