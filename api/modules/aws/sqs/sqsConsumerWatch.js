// Load the AWS SDK for Node.js
var AWS = require('aws-sdk')
// Set the region
AWS.config.update({
  region: 'us-east-1',
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_ACCESS_KEY
})

// Create an SQS service object
var sqs = new AWS.SQS({apiVersion: '2012-11-05'})

var queueURL = "https://sqs.us-east-1.amazonaws.com/00000000000/user-manager-find-users-response-queue"

var params = {
 QueueUrl: queueURL,
 VisibilityTimeout: 20,
 WaitTimeSeconds: 0
}

const getMessageSQS = async () => {
  return new Promise((resolve, reject) => {  
    sqs.receiveMessage(params, function(err, data) {

      if (err) {
        console.log("Receive Error", err)
        reject(err)
        return 
      } else if (data.Messages) {
        var deleteParams = {
          QueueUrl: queueURL,
          ReceiptHandle: data.Messages[0].ReceiptHandle
        }

        sqs.deleteMessage(deleteParams, function(err, data) {
          if (err) {
            console.log("Delete Error", err)
            reject(err)
            return            
          } else {
            console.log("Message Deleted", data)
            resolve(data)
          }
        })
      }
      setTimeout(() => {
        resolve('No message')
      },5000)
      
    })
  })
}

var validateCheck = 0
setInterval(() => {

  if (validateCheck === 0) {
    validateCheck = 1
    getMessageSQS().then(response => {
      console.log(response)
      validateCheck = 0
    }).catch(error => {
      console.log(error)
    })
  }
  

}, 1000)
