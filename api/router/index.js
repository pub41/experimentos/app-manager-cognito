const routes = require('express').Router()
const managerUser = require('../modules/aws/cognito/index')
const managerRoles = require('../modules/aws/dynamodb/aws-create')

routes.get('/create/:username/:password', async(req, res) => {
  let responseManage = await managerUser.create(req.params.username, req.params.password)
  return res.status(responseManage.status_code).json(responseManage)
})

routes.get('/update/:username/:password/:token', async(req, res) => {
  let responseManage = await managerUser.update(req.params.username, req.params.password, req.params.token)
  return res.status(responseManage.status_code).json(responseManage)
})

routes.get('/login/:username/:password', async(req, res) => {
  let responseManage = await managerUser.login(req.params.username, req.params.password)
  return res.status(responseManage.status_code).json(responseManage)
})

routes.get('/create-roles/:username', async(req, res) => {
  let responseManage = await managerRoles(req.params.username)
  return res.json(responseManage)
})

routes.post('/', async(req, res) => {
  return res.json({data: req.body})
})

routes.put('/:id', async(req, res) => {
  return res.json({data: req})
})

routes.delete('/:id', async(req, res) => {
  return res.json({data: req.params.id})
})

module.exports = routes
